const express = require('express');
const stockController = require('../controller/stockController');

const service = express();

module.exports = (config) => {
  const log = config.log();
  // Add a request logging middleware in development mode
  if (service.get('env') === 'development') {
    service.use((req, res, next) => {
      log.debug(`${req.method}: ${req.url}`);
      return next();
    });
  }

  // TO DO
  service.get('/stock-service/list', (req, res, next) => {
    return next('Not implemented');
  });

  service.get('/stock-service/:product_desc', async (req, res, next) => {
    try {

      const { product_desc } = req.params;

      var product = await stockController.check_stock(product_desc);
      res.status(200).json({stock : product}); 
      
    } catch (error) { 
      log.info(error);
      res.status(200).json({stock : false});
    }
  });

  // eslint-disable-next-line no-unused-vars
  service.use((error, req, res, next) => {
    res.status(error.status || 500);
    // Log out the error to the console
    log.error(error);
    return res.json({
      error: {
        message: error.message,
      },
    });
  });
  return service;
};
