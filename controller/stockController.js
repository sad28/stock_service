const config = require('../config/index');
const client = config.client;

async function check_stock(product_desc) { 
    try {

        // Connect to the MongoDB cluster
        await client.connect(); 
        // Make the appropriate DB calls
        const db = client.db();

        var product = await db.collection("products").findOne({desc: product_desc});

        if (product.stock == 0) {
            throw new Error("No hay stock del producto " + product.desc);
        } else { 
            return product;
        } 
        
    } finally {                   
        await client.close();
    } 
}

module.exports = { check_stock };